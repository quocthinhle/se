from html import entities
import re
import json
from datetime import datetime
import pytz
import unicodedata as ud

from .services.convert_number import word_to_num, convert_phone_number
from .helpers.common import CommonHelper
from .helpers.constants import SysEntity
from copy import deepcopy

with open('data/cities.json', 'r') as dataCity:
    cities = json.load(dataCity)
    cities = [ud.normalize('NFC', city) for city in cities]

with open('data/districts.json', 'r') as dataDistrict:
    dataDistricts = json.load(dataDistrict)
    districts = dataDistricts.keys()
    districts = [ud.normalize('NFC', district) for district in districts]

with open('data/wards.json', 'r') as dataWard:
    dataWards = json.load(dataWard)
    wards = dataWards.keys()
    wards = [ud.normalize('NFC', ward) for ward in wards]


class SysEntityService:
    @staticmethod
    def get_sys(text, excludeDefault = False):
        text = text.lower()

        entities = {}
        try:
            if excludeDefault is False:
                append_now_datetime(entities)
                append_text(entities, text)
            # url
            ret = extract_url(text)
            if ret.get("data", None):
                entities.update({SysEntity.URL: ret['data']})

            # phone number
            ret = extract_phone_number(text)
            if ret.get("data", None):
                entities.update({SysEntity.PHONE: ret['data']})

            # email
            ret = extract_email(text)
            if ret.get("data", None):
                entities.update({SysEntity.EMAIL: ret['data']})

            # duration
            ret = extract_duration(text)
            if ret.get("data", None):
                entities.update({SysEntity.DURATION: ret['data']})

            # number
            ret = extract_number(text)
            if ret.get("data", None):
                """
                Temporary Patch:
                 - Add `sys.integer` from `sys.number`.
                """
                int_nums, int_indices = [], []

                for idx, d in enumerate(ret["data"]):
                    n_leading_0 = 0
                    if d["value"].replace('0', '') != '': # not all zeros (like 0000)
                        while d["value"][n_leading_0] == '0': # find number of leading zero in string (0012345 -> 2)
                            n_leading_0 += 1
                    if isinstance(eval(d["value"][n_leading_0:]), int):
                        int_indices.append(idx)

                # Take away `int elements` from the original ret["data"]
                int_nums = [ret["data"][idx] for idx in int_indices]

                if int_nums:
                    entities.update({SysEntity.INTEGER: int_nums})
                
                ret_phone_number = extract_phone_number(text)
                if ret_phone_number.get("data", None):
                    tmp = []
                    phone_number_value = [phone_number['value'] for phone_number in ret_phone_number["data"]]
                    for phone_number_value_ in phone_number_value: 
                        for number_ in ret['data']:
                            if number_['value'] not in phone_number_value_:
                                tmp.append(number_)
                    ret["data"] = tmp
                entities.update({SysEntity.NUMBER: ret['data']})

            # city
            ret = extract_city(text)
            if ret.get("data", None):
                entities.update({SysEntity.CITY: ret['data']})

            entities = CommonHelper.remove_invalid(entities)
            entities = CommonHelper.remove_duplicate(entities)

        except Exception as e:
            return _refine(entities)
        
        return _refine(entities)

    def get_true_integer(self, entities):
        if SysEntity.INTEGER not in entities:
            return []

        integer_entities = sorted(deepcopy(entities[SysEntity.INTEGER]), key=lambda e: e['begin'])
        for i, e in enumerate(integer_entities):
            ori = e['origin']
            val = e['value']
            if val.endswith('000') and 'nghìn' not in ori and 'ngàn' not in ori:
                # revert integer auto-multiplied by 1000 (for money extraction purpose)
                val = val[:-3]
            if re.compile(r'\d00\d').match(val) and re.compile(r'\w+ (lẻ|linh) \w+').match(ori):
                val = val.replace('00', '0') # 'hai lẻ sáu' -> 2006 (year) -> 206 (true int)
            e['origin'] = ori
            e['value'] = val
            integer_entities[i] = e

        return integer_entities          


# region private function

def _refine(entities):
    num_ents = entities.get(SysEntity.NUMBER, None)

    if not num_ents:
        return entities

    def _is_intersection(num, date):
        if num['begin'] >= date['begin'] and \
            num['end'] <= date['end']:
            return True
        return False

    for entity_type in [SysEntity.DATE, SysEntity.TIME]:
        ents = entities.get(entity_type, None)

        if ents:
            temp_num_ents = num_ents.copy()
            idx = 0
            for _, n_ent in enumerate(temp_num_ents):
                for _, d_ent in enumerate(ents):
                    if _is_intersection(n_ent, d_ent):
                        num_ents.pop(idx)
                        idx -= 1
                        break
                idx += 1

            entities.update({SysEntity.NUMBER: num_ents})

    return entities


def _normalize(start_idx, end_idx, value, origin, confidence=100):

    return {
        'begin': start_idx,
        'end': end_idx,
        'value': value,
        'origin': origin,
        'confidence': confidence
    }


def _regex_matching(regex, text, _type=None):
    result = {
        'converted_text': text,
        'data': []
    }

    matches = re.finditer(regex.lower(), text.lower()) # Why regex need to be lower()?
    for m in matches:
        start_index = m.start()
        end_index = m.end()
        matched_text = text[start_index:end_index]
        converted_value = calculate_right_value_from_string(value=matched_text, _type=_type)
        text = text.replace(matched_text, '#'*len(matched_text))
        result['data'].append(_normalize(
                start_idx=start_index,
                end_idx=end_index,
                value=converted_value,
                origin=matched_text,
                confidence=100
            ))

    return result


def extract_url(text):
    regex = r"(?:https?://|www\.|[a-zA-Z]{1,}\.)(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"

    return _regex_matching(regex, text)


def extract_email(text):
    regex = r"[\w\.-]+@[\w\.-]+"

    return _regex_matching(regex, text)


# TODO
def get_origin_value(value):
    originValue = value
    if type == SysEntity.PHONE:
        regex = r"\(?[+]??\d{1,3}\)?\d{8,10}|\d{4}[\.\-\s]?\d{3}[\.\-\s]?\d{3}"
    elif type == SysEntity.EMAIL:
        regex = r"[\w\.-]+@[\w\.-]+"
    elif type == SysEntity.NUMBER:
        regex = r"\d+(?:[,.\s]\d{3})*"
    elif type == SysEntity.TIME:
        regex = r"\b\d{1,2}[\s]?(?:giờ|h)[\s]?(?:\d{1,2}[\s]?(?:phút|ph))?\b|(?:\d{1,2}[\s]?(?:giờ|h))?[\s]?\d{1,2}[\s]?(?:phút|ph)|\d{1,2}[\s]?:[\s]?\d{1,2}[\s]?(?:AM|am|PM|pm|sáng|chiều|tối)|\d{1,2}[\s]?(?:AM|am|PM|pm)"
    elif type == SysEntity.DATE:
        regex = r"(([0-2][0-9]|(3)[0-1])([\/\-])(((0)[0-9])|((1)[0-2]))([\/\-])\d{4})|(([0-2][0-9]|(3)[0-1])([\/\-])(((0)[0-9])|((1)[0-2])))"
    elif type == SysEntity.DURATION:
        regex = r"\d+[\s]?(?:ngày|tháng|năm|tuần|quý)\b"
    elif type == SysEntity.URL:
        regex = r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"

    return originValue


def append_now_datetime(data, timezone="Asia/Ho_Chi_Minh"):
    today = datetime.now(pytz.timezone(timezone))
    data[SysEntity.NOW] = [
        {
            "begin": -1,
            "end": -1,
            "origin": today.strftime("%H:%M:%S %d/%m/%Y"),
            "value": today.strftime("%H:%M:%S %d/%m/%Y"),
            "confidence": 100
        }
    ]
    data[SysEntity.CURRENT_DATE] = [
        {
            "begin": -1,
            "end": -1,
            "origin": today.strftime("%d/%m/%Y"),
            "value": today.strftime("%d/%m/%Y"),
            "confidence": 100
        }
    ]
    data[SysEntity.CURRENT_WEEKDAY] = [
        {
            "begin": -1,
            "end": -1,
            "origin": today.strftime("%a"),
            "value": today.strftime("%a"),
            "confidence": 100
        }
    ]
    data[SysEntity.CURRENT_TIME] = [
        {
            "begin": -1,
            "end": -1,
            "origin": today.strftime("%H:%M:%S"),
            "value": today.strftime("%H:%M:%S"),
            "confidence": 100
        }
    ]
    return data


def append_text(data, sentence):
    data[SysEntity.TEXT] = [
        {
            "begin": 0,
            "end": len(sentence) - 1,
            "value": sentence,
            "origin": sentence,
            "confidence": 100
        }
    ]
    return data


def extract_city(sentence):
    return extract_location(cities, sentence)


def extract_district(sentence):
    return extract_location(districts, sentence)


def extract_ward(sentence):
    return extract_location(wards, sentence)


def extract_location(data, sentence):
    result = {
        'convertedText': sentence,
        'data': []
    }

    sentence = ud.normalize('NFC', sentence)
    pattern = '|'.join(data).lower()
    regex = re.compile(pattern, re.IGNORECASE)
    for match in regex.finditer(sentence):
        begin = match.start()
        end = match.end()
        value = sentence[begin: end]
        result['data'].append({
            "begin": begin,
            "end": end,
            "value": value,
            "confidence": 99.999,
            "origin": value
        })
        sentence = sentence[:begin] + ('#' * len(value)) + sentence[end:]
    result['convertedText'] = sentence
    return result


def cal_days_from_string(value):
    month_convertation = {
        "ngày": "  *1",
        "tuần": "  *7",
        "tháng": " *30",
        "quí": " *90",
        "quý": " *90",
        "năm": "*365"
    }

    for pro, val in month_convertation.items():
        value = re.sub(r"%s\b" % pro, val, value)
    valueToExecute = '(' + value[:len(value)-4] + ')' + value[len(value)-4:]
    total = eval(valueToExecute)
    return total


def calculate_right_value_from_string(value, _type=None):
    try:
        if _type == SysEntity.DURATION:
            total = 0
            # convert to month
            total = cal_days_from_string(value)
            return total
        elif _type == SysEntity.NUMBER:
            return int(value)
        else:
            return value
    except Exception as e:
        return ''


def extract_entities(text,
                     regex_mask,
                     mask_text,
                     data,
                     entity_type):
    if not data:
        return dict({'convertedText': text,'data': []})

    index = indexFind = count = 0
    counting_threshold = 10
    entities = []
    mask_text = re.sub(r"(\d|(?<=\d)\,(?=\d))", '#', mask_text)
    while index < len(data) and count < counting_threshold:
        count += 1
        item = data[index]
        indexOfItem = text.find(item, indexFind)
        if indexOfItem != -1:
            start = indexOfItem
            end = indexOfItem + len(item)
            if start > 0 and re.match(r"[\s\.,:!]", text[start - 1]) is None:
                continue
            if end < len(text) and re.match(r"[\s,\.?!:]", text[end]) is None and entity_type != SysEntity.NUMBER:
                continue
            text = text[:start] + ('#' * len(item)) + text[end:]
            mask_text = mask_text[:start] + ('#' * len(item)) + mask_text[end:]
            entities.append({
                "begin": start,
                "end": end,
                "value": item,
                "origin": item,
                "confidence": 100
            })
            data.remove(item)
            indexFind = end
        else:
            index += 1
    matches = re.finditer(regex_mask, mask_text)
    text = text.replace('#', '~')
    index = 0
    if len(data) > 0:
        for m in matches:
            start = m.start()
            end = m.end()
            value = mask_text[start:end]
            if '#' not in value:
                continue

            origin = text[start:end]
            if '~' in origin:
                continue
            try:
                text = text[:start] + ('#' * len(value)) + text[end:]
                entities.append({
                    "begin": start,
                    "end": end,
                    "value": data[index],
                    "origin": origin,
                    "confidence": 100
                })
                index += 1
            except IndexError:
                continue

    return {
        'convertedText': text.replace('~', '#'),
        'data': entities
    }


def extract_phone_number(text):
    regex = r"(?:\(?\+?(?:84|0)\)?\s?[35789])\d{2}[\.\-\s]?\d{3}[\.\-\s]?\d{3}\b|(?:\+?(?:84|0)[35789])\d{1}[\.\-\s]?\d{3}[\.\-\s]?\d{4}\b|(?:02)\d{9}\b"
    regexMask = r"(?:#{1,5} ){9}#{1,5}"

    entities_phone_number = get_sys_entities(text, convert_phone_number, regex, regexMask)
    for phone_number in entities_phone_number['data']:
        phone_number['value'] = re.sub(r"[^\d]\(?(?:\+?84)\)?\s?", "0", phone_number['value'])
        phone_number['value'] = phone_number['value'].replace('(','').replace(')','').replace(' ','')
        phone_number['value'] = phone_number['value'].replace('.','')
        phone_number['value'] = phone_number['value'].replace('-','')

    return entities_phone_number


def extract_duration(text):
    regex_day = r"(?<!\d)[1-9]?[0-9]\s?(?:ngày)\b"      # number of days < 100
    regex_month = r"(?<!\d)[1-9]?[0-9]\s(?:tháng)\b"    # number of months < 100
    regex_year = r"(?<!\d)[1-9]?[0-9]\s(?:năm)\b"       # number of years < 100
    regex_week = r"(?<!\d)[1-9]?[0-9]\s(?:tuần)\b"      # number of weeks < 100
    regex_quarter = r"(?<!\d)[1-9]?[0-9]\s(?:quý)\b"    # number of quarters < 100
    # split the regex pattern here to easily change each limit differently if needed

    regex = rf"{regex_day}|{regex_month}|{regex_year}|{regex_week}|{regex_quarter}"
    regexMask = r"(#{2,}\s)*#{2,}\s?(?:ngày|tháng|năm|tuần|quý)\b"

    return get_sys_entities(text, word_to_num, regex, regexMask)


def extract_number(text):
    regex = r"\b\d+(?:[,.]\d{3})+\b|([\d.]*\d+)|\d+"
    regexMask = r"(?:(?<=thứ )#{2,})|#{2,}(?=[ $&+,:;=?@|'<>.^*()%!-/]|$)"

    return get_sys_entities(text, word_to_num, regex, regexMask, SysEntity.NUMBER)


def get_sys_entities(text, convert_func, regex, regexMask, entityType = SysEntity.TEXT):
    text = text.replace('#', '~')
    data = convert_func(text, entityType)
    maskText = data['maskText']
    convertedText = data['convertedText']
    result = {
        'convertedText': text,
        'data': []
    }
    data = re.findall(regex, convertedText)
    countData = len(data)

    if countData == 0:
        return result

    result = extract_entities(text, regexMask, maskText, data, entityType)
    result["convertedText"] = result['convertedText'].replace('~', '#')
    return result

#endregion