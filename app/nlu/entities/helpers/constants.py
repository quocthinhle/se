class SysEntity:
    # +84905969077 hoặc 0905123456 hoặc (+84)123456789
    PHONE = "sys.phone_number"
    # abc123@gmail.com
    EMAIL = "sys.email"
    # 300 ngày hoặc 12 tháng hoặc 3 năm
    DURATION = "sys.duration"
    # 25,000,000 hoặc 123456
    NUMBER = "sys.number"
    INTEGER = "sys.integer"
    # 10h30ph hoặc 12h56ph hoặc 0h0ph hoặc 7 giờ 50 phút hoặc 7AM hoặc 5:45PM
    TIME = "sys.time"
    # 20/10/2019
    DATE = "sys.date"
    # Tên
    FULL_NAME = "sys.full_name"
    # 64 tỉnh thành hoặc quận huyện của ĐN or HCM or HN
    CITY = "sys.city"
    DISTRICT = "sys.district"
    WARD = "sys.ward"
    PROVINCE = 'sys.province' # Tỉnh 
    URBAN_AREA = 'sys.urban_area' # Khu đô thị (KDC, Kp)
    ADDRESS_LANE = 'sys.address_lane' # Ngõ (Kiệt)
    ADDRESS_HAMLET = 'sys.address_hamlet' # Tổ (Thôn, Ấp)
    STREET_NUMBER = 'sys.street_number' # Số đường
    STREET = 'sys.street' # Tên đường
    TW_CITIES = { 'hà nội', 'hải phòng', 'đà nẵng', 'hcm', 'hồ chí minh', 'sài gòn', 'cần thơ' }
    LOCATION_PREFIX = {
        CITY: 'Thành phố',
        PROVINCE: 'Tỉnh', 
        DISTRICT: 'Quận (Huyện, Thị xã, Thành phố)',
        WARD: 'Phường (Xã, Thị trấn)',
        URBAN_AREA: 'Khu đô thị (KDC, Kp)', 
        ADDRESS_LANE: 'Ngõ (Kiệt)',
        ADDRESS_HAMLET: 'Tổ (Thôn, Ấp)',
        STREET_NUMBER: 'Số',
        STREET: "Đường"
    }
    # http://vnlp.ai
    URL = "sys.url"
    WEIGHT = "sys.weight"
    NOW = "sys.now"
    CURRENT_TIME = "sys.ctime"
    CURRENT_DATE = "sys.cdate"
    CURRENT_WEEKDAY = "sys.cweekday"
    TEXT = "sys.text"
    LICENSE_PLATE_NUMBER = "sys.license_plate_number"