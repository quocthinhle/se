from constants import SysEntity

class CommonHelper:
    @staticmethod
    def remove_duplicate(dict_t):
        new_dict = {}
        for k, arr in dict_t.items():
            arr.sort(key=lambda x: x['confidence'], reverse=True)
            new_array = []
            for entity in arr:
                val = entity["value"].replace("\b", "").replace(" .", "")
                origin = ""
                if "origin" in entity and entity["origin"]:
                    origin = entity["origin"]
                else:
                    origin = val
                confidence = 100
                if "confidence" in entity:
                    confidence = entity["confidence"]

                existed_entity = (
                    len([x for x in new_array if x["begin"] == entity["begin"]]) > 0
                )

                if not existed_entity:
                    tmp_obj = {
                        "value": val,
                        "confidence": confidence,
                        "origin": origin,
                        "begin": entity["begin"],
                        "end": entity["end"],
                    }
                    new_array.append(tmp_obj)

            if len(new_array) > 0:
                new_dict[k] = new_array

        return new_dict

    @staticmethod
    def remove_invalid(entities):
        for k in entities.keys():
            if k == SysEntity.TEXT:
                continue
            entities[k] = [e for e in entities[k] if ('begin' in e and 'end' in e and 'origin' in e and e['end']!=0 and e['origin']!='')]
        entities = {k: v for k, v in entities.items() if v}
        return entities

