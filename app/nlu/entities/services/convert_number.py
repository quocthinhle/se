import re

from common.constants import SysEntity
from helpers.logger import LoggerHelper as logger


number_pronounces = {
    "trăm": "* 100 +",
    "nghìn": "* 1000 +",
    "ngàn": "* 1000 +",
    "k": "* 1000 +",
    "triệu": "* 1000000 +",
    "củ": "* 1000000 +",
    "chai": "* 1000000 +",
    "tr": "* 1000000 +",
    "tỷ": "* 1000000000 +",
    "tỉ": "* 1000000000 +",
    "mười một": "11",
    "mười hai": "12",
    "mười ba": "13",
    "mười bốn": "14",
    "mười lăm": "15",
    "mười sáu": "16",
    "mười bảy": "17", "mười bẩy": "17",
    "mười tám": "18",
    "mười chín": "19",
    "mươi lăm": "##5",
    "mươi tư": "##4",
    "lăm": "##5", "nhăm": "##5",
    "tư": "##4",
    "một": "#1",
    "hai": "#2", "hăm": "#2",
    "ba": "#3",
    "bốn": "#4",
    "sáu": "#6",
    "bảy": "#7", "bẩy": "#7",
    "tám": "#8",
    "chín": "#9",
    "mười": "#10",
    "mươi": "* 10 +",
    "chục": "* 10 +",
    "mốt": "1",
    "phẩy": "#.",
    "phết": "#.",
    "chấm": "#.",
    "lẻ": "+",
    "linh": "+",
    "rưỡi": "#1/2",
    "năm": "#5",    
    "không": "#0",
}

phone_number_pronounces = {
    "không": "#0",
    "mươi": "#0",
    "lẻ": "#0",
    "linh": "#0",
    "một": "#1",
    "mốt": "#1",
    "mười": "#1",
    "hai": "#2",
    "hăm": "#2",
    "ba": "#3",
    "bốn": "#4",
    "tư": "#4",
    "năm": "#5",
    "lăm": "#5",
    "sáu": "#6",
    "bảy": "#7",
    "bẩy": "#7",
    "tám": "#8",
    "chín": "#9"
}


def cal_decimal_group(txt, decimalGroup, previousIdx=0):
    result = 0
    if decimalGroup in txt[previousIdx:]:
        idx = txt.rindex(decimalGroup)
        temp = '1' if txt[previousIdx:idx] == '' else txt[previousIdx:idx]
        separatedTxt = ("(" + temp + ")"
                    + txt[idx: idx + len(decimalGroup)])
        result = eval(separatedTxt)
        previousIdx = idx + len(decimalGroup) + 1
    return {"total": result, "previousIdx": previousIdx}


def cal_amount_from_string(value):
    # separate word into group: billion, million, thousand, hundred
    previousIdx = 0
    billionIdx = 0
    millionIdx = 0
    thousandIdx = 0
    hundredIdx = 0
    # hundreds of billions
    total = 0
    hundreds_of_billions = "*100000000000"
    result = cal_decimal_group(value, hundreds_of_billions, previousIdx)
    total += result["total"]

    # tens of billions
    tens_of_billions = "*10000000000"
    result = cal_decimal_group(value, tens_of_billions, result["previousIdx"])
    total += result["total"]

    # billion
    billionWord = "*1000000000"
    result = cal_decimal_group(value, billionWord, result["previousIdx"])
    total += result["total"]

    # hundreds of millions
    hundreds_of_millions = "*100000000"
    result = cal_decimal_group(value, hundreds_of_millions, result["previousIdx"])
    total += result["total"]

    # tens of millions
    tens_of_millions = "*10000000"
    result = cal_decimal_group(value, tens_of_millions, result["previousIdx"])
    total += result["total"]

    # million
    millionWorld = "*1000000"
    result = cal_decimal_group(value, millionWorld, result["previousIdx"])
    total += result["total"]

    # hundred thousand
    thousandWorld = "*100000"
    result = cal_decimal_group(value, thousandWorld, result["previousIdx"])
    total += result["total"]

    #  tens of thousands
    thousandWorld = "*10000"
    result = cal_decimal_group(value, thousandWorld, result["previousIdx"])
    total += result["total"]

    # thousand
    thousandWorld = "*1000"
    result = cal_decimal_group(value, thousandWorld, result["previousIdx"])
    total += result["total"]

    # hundred
    hundredWord = "*100"
    result = cal_decimal_group(value, hundredWord, result["previousIdx"])
    total += result["total"]

    # rest
    restTxt = value[result["previousIdx"]:]
    if restTxt:
        try:
            total += eval(restTxt)
        except Exception as e:
            restTxt_ = re.sub(r"(?:(?<=[\*\+])0+|^0+)(?=\d)", "", restTxt) # remove leading zeros
            total += eval(restTxt_)
    return total


def word_to_number(data, entityType=SysEntity.TEXT):
    original_text = convertedText = data['convertedText']
    maskText = data['maskText']
    for key in number_pronounces.keys():
        if key == 'rưỡi':
            if re.findall(rf'((?<=triệu\s)|(?<=củ\s)){key}', original_text):
                maskText = re.sub(rf'(?<=#\s){key}', '#'*len(key), maskText)
                convertedText = re.sub(rf'(?<=\* 1000000 \+\s){key}(?!\snăm)(?!\s#\d)', '500000', convertedText)
                convertedText = re.sub(rf'(?<=\* 1000000 \+\s){key}', '5', convertedText)

            elif re.findall(rf'(?<=trăm\s){key}', original_text):
                maskText = re.sub(rf'(?<=#\s){key}', '#'*len(key), maskText)
                convertedText = re.sub(rf'(?<=\* 100000 \+\s){key}(?!\snăm)(?!\s\d)', '50000', convertedText)
                convertedText = re.sub(rf'(?<=\* 100 \+\s){key}', '50', convertedText)

            # một rưỡi → chín rưỡi
            elif re.findall(rf'(?:(?<=một\s)|(?<=hai\s)|(?<=ba\s)|(?<=bốn\s)|(?<=năm\s)|(?<=sáu\s)|(?<=bảy\s)|(?<=bẩy\s)|(?<=tám\s)|(?<=chín\s)){key}', original_text):
                maskText = re.sub(rf'(?<=\s){key}', '#'*len(key), maskText)
                convertedText = re.sub(rf'(?:(?<=\d\s)|(?<=năm\s)){key}', '#. #5', convertedText) # order of 'năm' is matter

            continue

        if key == 'năm':
            if re.findall(rf'((?<=triệu\s)|(?<=củ\s)){key}(?!\schục|\smươi|\smốt|\shai|\sba|\sbốn|\stư|\slăm|\ssáu|\sbảy|\sbẩy|\stám|\schín|\străm|\striệu|\scủ)', original_text): # fix một triệu năm
                maskText = re.sub(rf'(?<=#\s){key}', '#'*len(key), maskText)
                convertedText = re.sub(rf'(?<=\* 1000000 \+\s){key}', '500000', convertedText)
            if re.findall(rf"(?<=[\d{1,2}] ){key}(?= \*)", convertedText): # triệu rưỡi năm chục
                maskText = re.sub(regex, len(key) * '#', maskText)
                convertedText = re.sub(rf"(?<=[\d{1,2}] ){key}(?= \*)", "#5", convertedText)
            else:
                regex_patterns = [
                    r"năm(?: năm){2,}", # >=thrice "năm"
                    r"(?<!tháng )năm(?: năm)+(?= #\d*)", # >=twice "năm" following with number
                    r"(?<=(?<!tháng )(?<!cách đây )(?<!trước đây )#\d )năm(?: năm)*(?! trước| sau| tới)", # leading number before single/multiple "năm"
                    r"((((?<![\d{1,2}] )(?<!một )(?<!hai )(?<!ba )(?<!bốn )(?<!năm )(?<!sáu )(?<!bảy )(?<!bẩy )(?<!tám )(?<!chín )(?<!mười )(?<!mười một )(?<!mười hai ))|^))(?<!tháng ## )(?<!tháng ### )(?<!tháng #### )(?<!tháng ######## )(?<!tháng năm )(?<!tháng \d )(?<!tháng \d\d )(?<!cách đây ### )(?<!trước đây ### )(?<!ngày tháng )năm(?! *\d{4}| nay| trước| ngoái| qua| tê| sau| tới| kia| vừa rồi)", # only if no number before "năm"
                    r"(?<=thứ )năm" # "thứ năm"
                ]
                for regex in regex_patterns:
                    # convertedText
                    matched_string = list(re.finditer(regex, convertedText))
                    all_words = convertedText.split(' ')
                    start = 0
                    for i in range(len(all_words)):
                        temp = len(all_words[i])
                        if all_words[i] == 'năm' and any(start >= item.start() and start <= item.end() - 3 for item in matched_string):
                            all_words[i] = number_pronounces[key] # literal '#5'
                        start += temp + 1
                    convertedText = ' '.join(all_words)

                    # maskText
                    matched_string = list(re.finditer(regex, maskText))
                    all_words = maskText.split(' ')
                    start = 0
                    for i in range(len(all_words)):
                        temp = len(all_words[i])
                        if all_words[i] == 'năm' and any(start >= item.start() and start <= item.end() - 3 for item in matched_string):
                            all_words[i] = '#' * len(all_words[i]) # literal '###'
                        start += temp + 1
                    maskText = ' '.join(all_words)

            continue

        if key == 'không':

            if re.findall(rf'(?:(?<=một\s)|(?<=hai\s)|(?<=ba\s)|(?<=bốn\s)|(?<=năm\s)|(?<=sáu\s)|(?<=bảy\s)|(?<=bẩy\s)|(?<=tám\s)|(?<=chín\s)|(?<=chục\s)|(?<=trăm\s)|(?<=triệu\s)|(?<=củ\s)|(?<=nghìn\s)|(?<=ngàn\s)){key}', original_text):
                maskText = re.sub(rf'(?<=#\s){key}', '#'*len(key), maskText)
                convertedText = re.sub(rf'(?<=\d\s|\+\s){key}', number_pronounces[key], convertedText)

            convertedText_old = ''
            while (convertedText != convertedText_old):
                convertedText_old = convertedText
                # handle multiple leading zeros (000012345)
                if re.findall(rf'{key}(?=\smột|\shai|\sba|\sbốn|\snăm|\ssáu|\sbảy|\sbẩy|\stám|\schín)', original_text):
                    maskText = re.sub(rf'{key}(?=\s#)', '#'*len(key), maskText)
                    convertedText = re.sub(rf'{key}(?=\s#)', number_pronounces[key], convertedText)
                # handle multiple trailing zeros (12345000)
                if re.findall(rf'(?:(?<=một\s|hai\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s){key})|(?:(?<=ba\s){key})|(?:(?<=chín\s){key})', original_text):
                    maskText = re.sub(rf'(?<=#\s){key}', '#'*len(key), maskText)
                    convertedText = re.sub(rf'(?<=#\d\s){key}', number_pronounces[key], convertedText)

            if re.findall(rf'{key}(?=\străm)', original_text):
                maskText = re.sub(rf'{key}(?=\s#)', '#'*len(key), maskText)
                convertedText = re.sub(rf'{key}(?=\s#)', number_pronounces[key], convertedText)

            regex = r"(?<=# |\d )không(?= |$|.|#)"
            maskText = re.sub(regex, len(key) * '#', maskText)
            convertedText = re.sub(
                rf'(?<=\+\s|\d ){key}(?=\s\*| #)', number_pronounces[key], convertedText)
            
            # Update convertedText and maskText for special cases with 'năm năm' before or after 'không'
            if re.findall(rf'(?:{key} )?năm năm(?: {key})?', original_text):
                # 'năm năm' before 'không'
                convertedText = re.sub(r"(?<!tháng )(?<=#5\s)năm(?=\s#0)", '#5', convertedText)
                maskText = re.sub(r"(?<!tháng )(?<=#{3}\s)năm(?=\s#{5})", '###', maskText)
                # 'không' before 'năm năm'
                convertedText = re.sub(r"(?<=(?<!tháng )(?<!cách đây )(?<!trước đây )#0 #5 )năm(?! trước| sau| tới)", '#5', convertedText)
                maskText = re.sub(r"(?<=(?<!tháng )(?<!cách đây )(?<!trước đây )#{5} #{3} )năm(?! trước| sau| tới)", '###', maskText)

            continue

        if key in ['chục', 'trăm', 'nghìn', 'ngàn', 'triệu', 'củ', 'tỷ', 'tỉ']:
            required_prefix = r'(?:(?<=không)|(?<=một)|(?<=hai)|(?<=ba)|(?<=bốn)|(?<=tư)|(?<=năm)|(?<=sáu)|(?<=bảy)|(?<=bẩy)|(?<=tám)|(?<=chín)|(?<=mười)|(?<=mươi)|(?<=mốt)|(?<=lăm)|(?<=trăm)|(?<=chục)|(?<=rưỡi)|(?<=\d{1})|(?<=#)|(?<=\+))'

            if key == 'chục':
                if re.findall(rf'{key}(?=(?:\smốt|\shai|\sba|\sbốn|\stư|\snăm|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)\s{key})', original_text): # fix một chục năm chục
                    maskText = re.sub(rf'{key}\s(?=(?:#+|năm)\s{key})', '#'*len(key) + '|', maskText) # add separator between two same-level numbers: một chục | năm chục
                    convertedText = re.sub(rf'{key}(?=(?:\s#\d|\snăm)\s{key})', number_pronounces[key] + ' * 1000 |', convertedText)
                if re.findall(rf'(?:(?<=một\s)|(?<=hai\s)|(?<=ba\s)|(?<=bốn\s)|(?<=năm\s)|(?<=sáu\s)|(?<=bảy\s)|(?<=bẩy\s)|(?<=tám\s)|(?<=chín\s)|(?<=mười\s)){key}(?!\snghìn|\sngàn)', original_text):
                    maskText = re.sub(rf'(?<=#\s){key}', '#'*len(key), maskText)
                    if (' * 1000 + ' not in convertedText) and re.findall(rf'{key}(?!\stỷ|\stỉ|\striệu|\scủ)', original_text): # fix 'ba chục triệu'
                        convertedText = re.sub(rf'(?:(?<=\d\s)|(?<=năm\s)){key}', number_pronounces[key] + ' * 1000 +', convertedText) # order of 'năm' is matter
                    else: # if 'nghìn' or 'ngàn' already exists then no need to multiply by 1000
                        convertedText = re.sub(rf'(?:(?<=\d\s)|(?<=năm\s)){key}', number_pronounces[key], convertedText) # order of 'năm' is matter
                elif re.findall(rf'{key}(?:\stỷ|\stỉ|\striệu|\scủ|\snghìn|\sngàn)', original_text):
                    if (' * 1000 +' not in convertedText) and re.findall(rf'{key}(?:\snghìn|\sngàn)', original_text): # fix 'ba chục triệu'
                        convertedText = re.sub(rf'(?<! #\d\s|năm\s){key}', ' 1 ' + number_pronounces[key] + ' * 1000 +', convertedText) # order of 'năm' is matter
                    else: # if 'nghìn' or 'ngàn' already exists then no need to multiply by 1000
                        convertedText = re.sub(rf'(?<! #\d\s|năm\s){key}', ' 1 ' + number_pronounces[key], convertedText) # order of 'năm' is matter
                elif re.findall(rf'(?<!một\s)(?<!hai\s)(?<!ba\s)(?<!bốn)(?<!năm)(?<!sáu)(?<!bảy)(?<!bẩy)(?<!tám)(?<!chín){key}', original_text): # fix chục ngày/chục cái
                    maskText = re.sub(rf'(?<!#\s){key}', '#'*len(key), maskText)
                    convertedText = re.sub(rf'(?<!\d\s)(?<!năm\s){key}', '10', convertedText) # order of 'năm' is matter

            if key == 'trăm':
                if re.findall(rf'{key}(?=(?:\smốt|\shai|\sba|\sbốn|\stư|\snăm|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)\s{key})', original_text): # fix một trăm năm trăm
                    maskText = re.sub(rf'{key}\s(?=(?:mốt|hai|ba|bốn|tư|năm|rưỡi|sáu|bảy|bẩy|tám|chín)\s{key})', '#'*len(key) + '|', maskText) # add separator between two same-level numbers: một trăm | năm trăm
                    convertedText = re.sub(rf'{key}(?=(?:\smốt|\shai|\sba|\sbốn|\stư|\snăm|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)\s{key})',  '* 100000 |', convertedText)
                if (not set(convertedText.split()) & set({'nghìn', 'ngàn'})) \
                    and re.findall(rf'{key}(?!(\s\w+)*\stỷ|(\s\w+)*\stỉ|(\s\w+)*\striệu|(\s\w+)*\scủ|\smười|\slẻ|\slinh)', original_text) \
                    and not re.findall(r'trăm(?:\smươi|\smười|\slẻ|\slinh|\smột|\smốt|\shai|\shăm|\sba|\sbốn|\stư|\snăm|\slăm|\ssáu|\sbảy|\sbẩy|\stám|\schín){0,3}(?:\sđồng|\schấm|\sphẩy|\sphết)', original_text): # fix 'sáu trăm đồng', 'ba trăm triệu', 'một triệu hai trăm'
                    convertedText = re.sub(
                            rf"(?:(?<={required_prefix}\s)|(?<=\d\s))?{key}", "* 100000 +", convertedText)
                else: # if 'nghìn' or 'ngàn' or 'đồng' already exists then no need to multiply by 1000
                    convertedText = re.sub( # fix 'bốn trăm mấy nghìn'
                            rf"(?:(?<={required_prefix}\s)|(?<=\d\s)){key}(?=\smấy\snghìn|\smấy\sngàn)", "* 100000", convertedText)
                    convertedText = re.sub( # fix 'trăm mấy nghìn'
                            rf"{key}(?=\smấy\snghìn|\smấy\sngàn)", "100000", convertedText)
                    convertedText = re.sub( # fix 'bốn trăm mấy triệu/củ'
                            rf"(?:(?<={required_prefix}\s)|(?<=\d\s)){key}(?=\smấy\striệu|\smấy\scủ)", "* 100000000", convertedText)
                    convertedText = re.sub( # fix 'trăm mấy triệu/củ'
                            rf"{key}(?=\smấy\striệu|\smấy\scủ)", "100000000", convertedText)
                    convertedText = re.sub( # fix 'bốn trăm mấy tỷ/tỉ'
                            rf"(?:(?<={required_prefix}\s)|(?<=\d\s)){key}(?=\smấy\stỷ|\smấy\stỉ)", "* 100000000000", convertedText)
                    convertedText = re.sub( # fix 'trăm mấy tỷ/tỉ'
                            rf"{key}(?=\smấy\stỷ|\smấy\stỉ)", "100000000000", convertedText)
                    maskText = re.sub(r"mấy", '#'*len('mấy'), maskText)
                    convertedText = re.sub(
                            rf"(?:(?<={required_prefix}\s)|(?<=\d\s)){key}", "* 100 +", convertedText)
                    convertedText = re.sub( # fix 'trăm nghìn'
                            rf"{key}", "100 +", convertedText)

                if re.findall(rf'{key}((?=\srưỡi nghìn)|(?=\srưỡi ngàn)|(?=\srưỡi triệu)|(?=\srưỡi củ)|(?=\srưỡi tỷ)|(?=\srưỡi tỉ))', original_text):
                    maskText = re.sub(rf'{key}((?=\srưỡi nghìn)|(?=\srưỡi ngàn)|(?=\srưỡi triệu)|(?=\srưỡi củ)|(?=\srưỡi tỷ)|(?=\srưỡi tỉ))', '#'*len(key), maskText)
                    convertedText = re.sub(rf'{key}((?=\srưỡi nghìn)|(?=\srưỡi ngàn)|(?=\srưỡi triệu)|(?=\srưỡi củ)|(?=\srưỡi tỷ)|(?=\srưỡi tỉ))',  '* 100 +', convertedText)
                elif re.findall(rf'{key}(?=\smốt|\shai|\sba|\sbốn|\stư|\snăms|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)', original_text):
                    maskText = re.sub(rf'{key}(?=\smốt|\shai|\sba|\sbốn|\stư|\snăms|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)', '#'*len(key), maskText)
                    convertedText = re.sub(rf'{key}(?=\smốt|\shai|\sba|\sbốn|\stư|\snăms|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)',  '* 100000 +', convertedText)


            if key in ['triệu', 'củ']:
                if re.findall(rf'{key}(?=(?:\smốt|\shai|\sba|\sbốn|\stư|\snăm|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)\s{key})', original_text): # fix một triệu năm triệu
                    maskText = re.sub(rf'{key}\s(?=(?:mốt|hai|ba|bốn|tư|năm|rưỡi|sáu|bảy|bẩy|tám|chín)\s{key})', '#'*len(key) + '|', maskText) # add separator between two same-level numbers: một triệu | năm triệu
                    convertedText = re.sub(rf'{key}(?=(?:\smốt|\shai|\sba|\sbốn|\stư|\snăm|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)\s{key})',  '* 1000000 |', convertedText)
                if re.findall(rf'{key}(?=\smốt|\shai|\sba|\sbốn|\stư|\snăm|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)', original_text):
                    maskText = re.sub(rf'{key}(?=\smốt|\shai|\sba|\sbốn|\stư|\snăm|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)', '#'*len(key), maskText)
                    convertedText = re.sub(rf'{key}(?=\smốt|\shai|\sba|\sbốn|\stư|\snăm|\srưỡi|\ssáu|\sbảy|\sbẩy|\stám|\schín)',  '* 1000000 +', convertedText)
            maskText = re.sub(rf"{required_prefix} {key}\b", ' ' + '#'*(len(key) + 0), maskText)
            maskText = re.sub(rf"\b{key}\b", '#' * len(key), maskText) # in case 'trăm nghìn'
            convertedText = re.sub(
                    rf"(?:(?<={required_prefix}\s)|(?<=\d\s)){key}\b", number_pronounces[key], convertedText)

            continue

        if key in ["k", "chai", "tr"]:
            if re.findall(rf"(?<=\d|#){key}\b", maskText):
                maskText = re.sub(rf"(?<=\d|#){key}\b", "#"*len(key), maskText)
                convertedText = re.sub(rf"(?<=\d|#){key}\b", number_pronounces[key], convertedText)

            elif re.findall(rf"(?<=\d\s|#\s){key}\b", maskText):
                maskText = re.sub(rf"(?<=\d\s|#\s){key}\b", "#"*(len(key) + 1), maskText)
                convertedText = re.sub(rf"(?<=\d\s|#\s){key}\b", number_pronounces[key], convertedText)

            continue

        if 'mười ' in key:
            temp = key.split(' ')

            maskText = re.sub(rf"\b{key}\b", len(
                temp[0]) * '#' + '#' + len(temp[1]) * '#', maskText)
            convertedText = re.sub(
                rf"\b{key}\b", number_pronounces[key], convertedText)
            continue

        if key in ['một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'bẩy', 'tám', 'chín']:
            if re.findall(rf'((?<=triệu\s)|(?<=củ\s)){key}', original_text):
                if re.findall(rf'{key}(?!\schục|\smươi|\smốt|\shai|\sba|\sbốn|\stư|\slăm|\ssáu|\sbảy|\sbẩy|\stám|\schín)', original_text):
                    maskText = re.sub(rf'(?<=#\s){key}\b(?!\s#|\s\*)', '#'*len(key), maskText)
                    convertedText = re.sub(rf'(?<=\* 1000000 \+ ){key}\b(?! \*| #|\schục)', number_pronounces[key] + ' * 100000 +', convertedText) #  order of 'chục' is matter

            if re.findall(rf'(?<=trăm\s){key}', original_text):
                if re.findall(rf'{key}(?!\schục|\smươi|\smốt|\shai|\sba|\sbốn|\stư|\slăm|\ssáu|\sbảy|\sbẩy|\stám|\schín)', original_text):
                    maskText = re.sub(rf'(?<=#\s){key}\b(?!\schục|\smươi|\smốt|\shai|\sba|\sbốn|\stư|\slăm|\ssáu|\sbảy|\sbẩy|\stám|\schín| #| \*)', '#'*len(key), maskText)
                    convertedText = re.sub(rf'(?<=\* 100000 \+ ){key}\b(?! \*| #|\smươi|\schục)', number_pronounces[key] + '* 10000 +', convertedText)
                    convertedText = re.sub(rf'(?<=\* 100 \+ ){key}\b(?! #|\smươi|\schục)', number_pronounces[key] + '* 10 +', convertedText) # fix 'ba trăm hai ngàn'
            
            elif re.findall(rf'(?<=trăm\s)(?:mười\s|hai\s|hăm\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s)(?:mươi\s)?{key}', original_text): # fix 'ba trăm hai ba'
                convertedText = re.sub(rf'(?<=\* 100000 \+ )((?:mười\s|hai\s|hăm\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s|#\d\s)(?:mươi\s)?{key}\b)(?! \*| #|\smươi|\schục)', r'\1 * 1000 +', convertedText)
            
            maskText = re.sub(rf'\b{key}\b', len(key) * '#', maskText)
            convertedText = re.sub(rf'\b{key}\b', number_pronounces[key], convertedText)
            continue

        if key in ['mốt', 'tư']:
            if re.findall(rf'(?:(?<=mươi\s)|(?<=hăm\s)|(?<=hai\s)|(?<=ba\s)|(?<=bốn\s)|(?<=năm\s)|(?<=sáu\s)|(?<=bảy\s)|(?<=bẩy\s)|(?<=tám\s)|(?<=chín\s)){key}', original_text):
                if re.findall(rf'(?<=trăm\s)(?:hai\s|hăm\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s)(?:mươi\s)?{key}', original_text): # fix 'ba trăm hai mốt'
                    maskText = re.sub(rf'\b{key}\b', '#'*len(key), maskText)
                    convertedText = re.sub(rf'(?<=\* 100000 \+ )((?:hai\s|hăm\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s|#\d\s)(?:mươi\s)?)(?:{key}|{number_pronounces[key]})(?! \*| #|\smươi|\schục)', rf'\1 {number_pronounces[key]} * 1000 +', convertedText)

                maskText = re.sub(rf'(?:(?<=#\s)|(?<=mươi\s)|(?<=hăm\s)|(?<=hai\s)|(?<=ba\s)|(?<=bốn\s)|(?<=năm\s)|(?<=sáu\s)|(?<=bảy\s)|(?<=bẩy\s)|(?<=tám\s)|(?<=chín\s)){key}', '#'*len(key), maskText)
                convertedText = re.sub(
                    rf"(?:(?<=mươi\s)|(?<=hăm\s)|(?<=hai\s)|(?<=ba\s)|(?<=bốn\s)|(?<=năm\s)|(?<=sáu\s)|(?<=bảy\s)|(?<=bẩy\s)|(?<=tám\s)|(?<=chín\s)|(?<=\* 10 \+\s)|(?<=#\d\s)){key}",
                    number_pronounces[key],
                    convertedText)

            if re.findall(rf'((?<=triệu\s)|(?<=củ\s)){key}', original_text):
                maskText = re.sub(rf'(?<=#\s){key}', '#'*len(key), maskText)
                convertedText = re.sub(rf'(?:(?<=triệu\s)|(?<=\* 1000000 \+\s)){key}', number_pronounces[key] + ' * 100000 +', convertedText)  # fix 'ba triệu tư/mốt ba mươi sáu ngàn'

            if re.findall(rf'(?<=trăm\s){key}', original_text):
                maskText = re.sub(rf'(?<=#\s){key}', '#'*len(key), maskText)
                convertedText = re.sub(rf'(?:(?<=trăm\s)|(?<=\* 100000 \+\s)){key}', '10000' if key=='mốt' else '40000', convertedText)
            
            if re.findall(rf'(?:(?<=lẻ\s)|(?<=linh\s)){key}', original_text): # hai trăm lẻ tư -> 204 
                maskText = re.sub(rf'(?:(?<=lẻ\s)|(?<=linh\s)){key}', '#'*len(key), maskText)
                convertedText = re.sub(rf'(?:(?<=lẻ\s)|(?<=linh\s)){key}', '#1' if key=='mốt' else '#4', convertedText)
            
            if re.findall(r'(?:(?<=thứ\s)|(?<=tháng\s))tư', original_text) and key=='tư':
                maskText = re.sub(r'(?:(?<=thứ\s)|(?<=tháng\s))tư', '##', maskText)
                convertedText = re.sub(r'(?:(?<=thứ\s)|(?<=tháng\s))tư', '4', convertedText)

            if re.findall(r'(?:(?<=chấm\s)tư)', original_text):
                maskText = re.sub(rf'\b{key}\b', len(key) * '#', maskText)
                convertedText = re.sub(rf'\b(?<=chấm\s){key}\b', number_pronounces[key], convertedText) # một chấm tư tư => 1.4

            continue

        if key in ['lẻ', 'linh']: # fix 'hai lẻ hai', 'hai linh bảy', 'hai linh năm', 'hai lẻ tư'
            regex = rf"(?:(?<=#\d |## )(?:không )?{key}(?= #\d| #+))|(?:(?<=#2 |## )(?:không )?{key}(?= năm))"
            maskText = re.sub(regex, len(key) * '#', maskText)
            convertedText = re.sub(regex, ' * 1000 + ', convertedText)
   
        if key == 'lăm':
            if re.findall(rf'(?<=trăm\s)(?:mười\s|hai\s|hăm\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s)(?:mươi\s)?{key}', original_text): # fix 'ba trăm hai lăm'
                convertedText = re.sub(rf'(?<=\* 100000 \+ )((?:mười\s|hai\s|hăm\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s|#\d\s)(?:mươi\s)?(?:{key}|{number_pronounces[key]}))(?! \*| #|\smươi|\schục)', r'\1 * 1000 +', convertedText)
        
        if key == 'nhăm':
            if re.findall(rf'(?<=trăm\s)(?:hai\s|hăm\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s)(?:mươi\s)?{key}', original_text): # fix 'hai nhăm'
                convertedText = re.sub(rf'(?<=\* 100000 \+ )((?:hai\s|hăm\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s|#\d\s)(?:mươi\s)?{key})(?! \*| #|\smươi|\schục)', r'\1 * 1000 +', convertedText)

        if key == 'mươi':
            if re.findall(rf'(?<=triệu )(?:hai\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s){key}(?! (nghìn|ngàn))', original_text): # fix 'triệu năm mươi'
                convertedText = re.sub(rf'(?<=\* 1000000 \+ )(hai\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s|#\d\s){key}(?! #\d| mốt)(?! nghìn| ngàn)', r'\1* 10000', convertedText)
            if re.findall(rf'(?<=trăm\s)(?:hai\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s){key}', original_text): # fix 'ba trăm hai mươi'
                convertedText = re.sub(rf'(?<=\* 100000 \+ )((?:hai\s|ba\s|bốn\s|năm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s|#\d\s){key})(?! \*| #|\smươi|\schục|\smốt)', r'\1 * 1000 +', convertedText)
            if re.findall(rf'mươi(?:\shai|\shăm|\sba|\sbốn|\snăm|\ssáu|\sbảy|\sbẩy|\stám|\schín)(?:\smươi|\smốt|\shai|\sba|\sbốn|\stư|\snăm|\slăm|\ssáu|\sbảy|\sbẩy|\stám|\schín)\b', original_text): # fix 'hai mươi hai mốt', 'bốn mươi năm mươi'
                convertedText = re.sub(rf'(?<=#\d ){key}(?= (?:#\d|năm) #+\d| (?:#\d|năm) mốt| (?:#\d|năm) mươi)', rf'#0', convertedText)
                maskText = re.sub(rf'(#+ ){key}( #+ #+)', rf'\1####\2', maskText)

        if key in ['phẩy', 'phết', 'chấm']:
            if re.findall(rf'(?:không\s|một\s|mốt\s|hai\s|ba\s|bốn\s|tư\s|năm\s|lăm\s|sáu\s|bảy\s|bẩy\s|tám\s|chín\s|mười\s|mươi\s|trăm\s){key}(?:\skhông|\smột|\shai|\sba|\sbốn|\stư|\snăm|\ssáu|\sbảy|\sbẩy|\stám|\schín|\smười)', original_text):
                maskText = re.sub(rf'{key}(?=\s\#)', '#' * len(key), maskText)
                convertedText = re.sub(rf'\b{key}\b', number_pronounces[key], convertedText)
        
        else:
            maskText = re.sub(rf'\b{key}\b', '#'*len(key), maskText)
            convertedText = re.sub(rf'\b{key}\b', number_pronounces[key], convertedText)
    
    # Fill symbol '#' into the gap between consequence '#'-string
    hashtag_strings =  re.finditer(r"(?<=# )#{1,}(?= #)", maskText)
    original_string = maskText
    list_original_string = list(original_string)
    _hashtag_strings = list(hashtag_strings)
    l = len(_hashtag_strings)
    for idx, match in enumerate(_hashtag_strings):
        list_original_string[match.start()-1] = "#"
        if idx == l - 1:
            list_original_string[match.end()] = "#"
    transformed_string = "".join(list_original_string)
    for match in re.findall(r'tháng #+ (?=(?:#+ ){2,})', maskText):    # fix sticked month-year "tháng sáu hai lẻ hai": "tháng ### ### ## ###" -> "tháng ##############"-> "tháng ### ##########"
        place = match[:-1] + '#'
        transformed_string = transformed_string.replace(place, match)
    maskText = transformed_string

    return {
        'convertedText': convertedText,
        'maskText': maskText
    }


def word_to_num(text, entityType=SysEntity.TEXT):
    original_text = converted_text = masked_text = text

    try:
        data = {
            "text": original_text,
            "convertedText": converted_text,
            "maskText": masked_text
        }
        data = word_to_number(data, entityType)

        converted_text = data['convertedText']
        converted_text = re.sub(r"(?:(?<=tháng \d\d|tháng #\d)|(?<=tháng \d)) #(?=\d)", "| ", converted_text) # add separator to fix sticked month-year "tháng sáu hai lẻ hai": tháng #6 #2 -> tháng #6| 2
        converted_text = re.sub(r"(?<=\d) #(?=\d)", "", converted_text)
        converted_text = re.sub(r"(?<=\d) ##(?=\d)", "", converted_text)
        converted_text = re.sub(r"(?<=\d) #(?=,)", "", converted_text)
        converted_text = re.sub(r"(?<=\d) \+ \* (?=\d)", " * ", converted_text)

        regex = r'(?! )[\d*+ \.,#]+(?<! )(?<!\.)(?<!,)'
        temp = converted_text

        matches = re.finditer(regex, converted_text)
        for match in matches:
            begin = match.start()
            end = match.end()
            matched_text = converted_text[begin:end].strip().strip('+').strip()
            matched_text = matched_text.replace(r'(?<=\d),', '.')
            matched_text = matched_text.replace('#', '')
            matched_text = matched_text.replace(' ', '')
            value_str = matched_text
            if set({'*', '+', '.'}) & set(matched_text):
                value_str = str(cal_amount_from_string(value_str))
            asterisk_str = (end - begin - len(value_str)) * '#'
            temp = temp[:begin] + value_str + asterisk_str + temp[end:]

        regex = r"(?<=năm )[\d ]{2,7}\d"
        temp = temp.replace('#', '')
        for match in re.finditer(regex, temp):
            begin = match.start()
            end = match.end()
            matched_text = temp[begin:end].replace(' ', '')
            asterisk_str = (end - begin - len(matched_text)) * '#'
            temp = temp[0:begin] + matched_text + asterisk_str + temp[end:]

        temp = temp.replace('#', '')
        temp = temp.replace('|', '') # remove separator (in case sticked month-year)
        mask_text = data['maskText']
        # add exception: split date and hour in sticked date-hour case (thứ bảy chín giờ -> thứ 79 giờ -> thứ 7 9 giờ)
        match = re.finditer(r"(?<=thứ )[2-7](?=(1?[0-9]\b|2[0-3]\b) giờ)", temp)
        for m in match:
            num_weekday = int(m[0])
            mask_weekday = '#' * (2 if num_weekday in [3, 4] else 3)
            temp = re.sub(rf"(?<=thứ {num_weekday})(?=(1?[0-9]\b|2[0-3]\b) giờ)", r" ", temp)
            mask_text = re.sub(rf"(?<=thứ ){mask_weekday}[#\s](?=#+ giờ)", rf"{mask_weekday} ", mask_text)
        data['maskText'] = mask_text
        data['convertedText'] = temp
        return data
    except Exception as e:
        logger.error(str(e))
        return data


def convert_phone_number(text, entityType=SysEntity.PHONE):
    convertedText = text
    maskText = text
    for key in phone_number_pronounces.keys():
        convertedText = re.sub(
            rf"\b{key}\b", phone_number_pronounces[key], convertedText)
        maskText = maskText.replace(key, len(key) * '#')

    regex = r"(?<=\d) ?#(?=\d)"
    convertedText = re.sub(regex, "", convertedText)
    convertedText = re.sub(r"#", "", convertedText)
    return {
        'convertedText': convertedText,
        'maskText': maskText
    }
