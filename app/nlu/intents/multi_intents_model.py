import os
import pandas as pd
import numpy as np
import csv
from typing import Optional, List, Union, Dict

from functools import reduce
import joblib
from pandas.core.common import flatten
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.multiclass import OneVsRestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics import classification_report
from sklearn.preprocessing import MaxAbsScaler, MultiLabelBinarizer
from sklearn.cluster import KMeans

intentModels = {}
max_cpu = -1

class SequentialModel:

    def __init__(self, oos_clf, ins_clf,
                 max_limit_intents=4,
                 suggestion_thres=50,
                 use_oos=False,
                 **kwargs):
        self.oos_clf = oos_clf
        self.ins_clf = ins_clf
        self.mlb = kwargs["mlb"]
        self.is_grouping = kwargs.get("is_grouping", False)

        # By default, max limit intents is half number of total intents.
        self._max_limit_intents = max_limit_intents
        self._use_oos = use_oos
        # By default, threshold for intent-suggestion is 50%
        self.suggestion_thres = suggestion_thres

    def predict(self, x, oos_threshold = 50, force_oov_other=True):
        pred = np.empty_like(x, dtype=object)

        if not isinstance(x, np.ndarray):
            x = np.array(x)

        if self._use_oos:
            # out of scope prediction
            oos_pred = self.oos_clf.predict(x)
            oos_scores = self.oos_clf.decision_function(x)
        else:
            """ this is a clever trick to bypass oos
            and then we get back previous version which:
                * not use oos binary.
                * always get top 4 intents by default.
            """
            # another trick: force any input sentence as oos intent
            # if its tfidf vector is zero vector
            
            if force_oov_other:
                nonzero_rows = set(self.ins_clf['tfidf'].transform(x).nonzero()[0])    
                is_zeros = np.array([0 if i in nonzero_rows else 1 for i in range(len(x))])
            else:
                is_zeros = [0] * len(x)
            
            oos_scores = self.ins_clf.decision_function(x)
            if len(oos_scores.shape) < 2:
                oos_scores = np.expand_dims(oos_scores, axis=1)
            oos_scores = np.vectorize(lambda i: 0 if _score2prob(i) > oos_threshold else 1)(oos_scores)
            oos_scores = oos_scores.prod(axis=1)
            oos_pred = np.vectorize(lambda i: 1 if int(i) == 1 else 0)(oos_scores)
        oos_ids = np.where(np.logical_or(oos_pred == 1, is_zeros == 1))[0]
        pred[oos_ids] = [[("other", 100)] for _, s in zip(
            oos_pred[oos_ids], oos_scores[oos_ids])]

        # in scope prediction
        if len(oos_ids) < len(pred):
            ins_ids = [i for i in range(len(x)) if i not in oos_ids]
            ins_sents = x[ins_ids]
            ins_scores = self.ins_clf.decision_function(ins_sents) # scores
            if len(ins_scores.shape) < 2:
                ins_scores = np.expand_dims(ins_scores, axis=0)
            ins_sorted_scores = np.array([x[::-1] for x in np.sort(ins_scores)])
            # labels ranking (descending)
            ins_sorted_ids = (-ins_scores).argsort(axis=1)
            # convert indices to labels
            ranked_labels = np.apply_along_axis(
                                func1d=lambda x: self.mlb.classes_[x],
                                axis=1,
                                arr=ins_sorted_ids)
            # set number of intents to be returned
            self._max_limit_intents = max(self._max_limit_intents, len(self.ins_clf.classes_) // 2)
            # assign back to the returned array
            pred[ins_ids] = [[(i, s) for i, s in zip(ii[:self._max_limit_intents], ss[:self._max_limit_intents])]
                    for ii, ss  in zip(ranked_labels, ins_sorted_scores)]

        return pred.tolist()

    def set_suggestion_threshold(self, suggestion_thres):
        self.suggestion_thres = suggestion_thres

    def suggest(self, sentence):
        '''
        Use trained model to suggest intents for sample.
        Particularly, intents predicted with bigger confidence score than threshold will be suggested.
        '''
        if not hasattr(self, 'suggestion_thres'):
            self.suggestion_thres = 50
        results = self.predict([sentence], oos_threshold = 0)[0]
        suggestions = [res[0] for res in results if _score2prob(res[1]) > self.suggestion_thres]
        return suggestions

def _cross_validate_generator(lst):
    for i in range(len(lst)):
        yield list(reduce(lambda ele, acc: acc+ele, lst[:i] + lst[i+1:], [])), lst[i]

def _clusters_maker(train_x, train_y, k):
    train_label_idx = [s.nonzero()[0] for s in train_y]
    df = pd.DataFrame.from_dict(
        {
            "Sentence":train_x,
            "Intent":train_label_idx,
            "Cluster": ["" for _ in train_y]
        }
    )

    for intent in range(train_y.shape[1]):
        subdf = df[list(map(lambda x: intent in x, df.Intent.values))]
        if len(subdf) <= k:
            subdf.Cluster = np.arange(len(subdf))
            df.update(subdf)
            continue

        sentences = subdf.Sentence.tolist()

        vectorize = TfidfVectorizer(token_pattern=r"(?u)<?\b\w+\b>?")
        X = vectorize.fit_transform(sentences)
        X = X.todense()

        kmeans = KMeans(n_clusters=k).fit(X)
        subdf.Cluster = kmeans.labels_
        df.update(subdf)

    lst = [df[df.Cluster == i].index.tolist() for i in range(k)]
    return lst

def grid_search(train_x,
            train_y,
            parameters,
            pipeline,
            num_fold=3,
            use_clustering_strategy=True):
    if use_clustering_strategy:
        _clusters = _clusters_maker(train_x, train_y, k=num_fold)

    # Hyperparameters tuning
    grid_search_tune = GridSearchCV(pipeline,
                                    parameters,
                                    cv=_cross_validate_generator(_clusters) if use_clustering_strategy else num_fold,
                                    n_jobs=max_cpu,
                                    verbose=10,
                                    error_score=0.0)
    grid_search_tune.fit(train_x, train_y)

    # Saving best estimator
    print("Saving best estimator")
    best_clf = grid_search_tune.best_estimator_
    print(f"BEST ESTIMATOR: {best_clf}\n")
    print(f"BEST PARAMS: {grid_search_tune.best_params_}")

    # Show result of some metrics evaluation.
    # Just a simple check for learning capability of
    # the trained model on training set.
    y_pred = best_clf.predict(train_x)
    print("[ INTENT ] [ GRID SEARCH ] Done")

    return best_clf

def train(botId, data_df):
    data_df = data_df.sample(frac=1, random_state=42)

    PROJECT_ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

    oos_df = pd.read_csv(os.path.join(PROJECT_ROOT_PATH, './data/oos.csv'))
    oos_data = oos_df["Text"].tolist()
    # check whether or not intent "other" being user training set
    # and if it exist then we add it into general oos data

    ins_df = data_df[data_df["other"] == 0] if "other" in data_df.columns else data_df
    ins_X = ins_df["Sentence"].tolist()
    X_in_scope, y_in_scope = ins_X, [0]*len(ins_X)
    X_out_scope, y_out_scope = oos_data, [1] * len(oos_data)
    X_oos = [*X_in_scope, *X_out_scope]
    y_oos = [*y_in_scope, *y_out_scope]


    # oos binary classifier (LR here for now)
    oos_clf_params = {
        "penalty": "l2",
        "dual": True,
        "random_state": 42,
        "solver": "liblinear",
        "n_jobs": max_cpu
    }

    oos_clf = LogisticRegression(**oos_clf_params)
    oos_vectorizer = TfidfVectorizer(token_pattern=r"(?u)<?\b\w+\b>?")
    oos_pipeline = Pipeline([
        ("tfidf", oos_vectorizer),
        ("scaler", MaxAbsScaler()),
        ("clf", oos_clf)
    ])
    # oos binary training
    oos_gridparams = {
        "tfidf__sublinear_tf": [True],
        "tfidf__max_df": ([0.25, 1.0]),
        "tfidf__ngram_range": [(1, 1), (1, 2)],
        "clf__C": [0.01, 0.1],
        "clf__class_weight": ["balanced"]
    }
    oos_gridsearch = GridSearchCV(oos_pipeline, oos_gridparams,
                                n_jobs=max_cpu,
                                verbose=10, cv=3, error_score=0)
    oos_gridsearch.fit(X_oos, y_oos)
    oos_clf = oos_gridsearch.best_estimator_


    # remove mono-sample-intent from training set
    def _one_drop(col):
        indices = data_df[(data_df[col].sum() == 1) & (data_df[col] == 1)].index
        if len(indices):
            data_df.drop(indices, axis="index", inplace=True)
            data_df.drop(col, axis="columns", inplace=True)

    for col in data_df.columns[1:]: _one_drop(col)

    # Get class labels and write it to disk for later use
    genres = list(data_df.drop(["Sentence"], axis=1).columns.values)

    label_str = './data' + str(botId) + 'label.txt'

    __file_label =  os.path.join(PROJECT_ROOT_PATH, label_str)

    label = open(__file_label, "w")
    for g in genres:
        label.write(g + "\n")

    data_x_untokenize = data_df[["Sentence"]].values
    data_x_array = []
    for k in data_x_untokenize:
        # note: we use tokenization in our pipeline
        text = k[0]
        data_x_array.append([text])
    data_x = np.array(data_x_array) # convert to numpy array
    data_y = data_df.drop(["Sentence"], axis=1).values
    x_train = data_x
    y_train = data_y[~np.isnan(data_y)].reshape((data_y.shape[0], -1)) 
    mlb = MultiLabelBinarizer(classes=genres).fit(y_train)
    X = [x[0].strip() for x in x_train.tolist()]

    # Make pipeline
    pipeline = Pipeline(
        [
            ("tfidf", TfidfVectorizer(token_pattern=r"(?u)<?\b\w+\b>?")),
            ("scaler", MaxAbsScaler()),
            ("clf", OneVsRestClassifier(LinearSVC(random_state=42),
                                        n_jobs=max_cpu)),
        ]
    )

    # Reduce grid-search space to shorten training time for large dataset
    if len(X) > 5000 or len(genres) > 50:
        parameters = {
            "tfidf__max_df": ([0.75]),
            "tfidf__ngram_range": [(1, 2)],
            "clf__estimator__C": [1],
            "clf__estimator__class_weight": ["balanced"],
        }
    elif len(X) > 100:
        parameters = {
            "tfidf__sublinear_tf": [True],
            "tfidf__max_df": ([0.25, 1.0]),
            "tfidf__min_df": ([1, 2]),
            "tfidf__ngram_range": [(1, 1), (1, 2)],
            "clf__estimator__C": [0.001, 0.01, 0.1, 0.5],
            "clf__estimator__class_weight": ["balanced"]
        }
    else:
        parameters = {
            "tfidf__ngram_range": [(1, 2)],
            "clf__estimator__C": [0.1, 0.5, 1],
            "clf__estimator__class_weight": ["balanced"]
        }

    X_train = X
    # Hyperparameter Tuning
    base_clf = grid_search(X_train,
                        y_train,
                        parameters,
                        pipeline)
    # Combine the two newly trained models above into unified one
    model = SequentialModel(oos_clf, base_clf, mlb=mlb, is_grouping=True)
    model.set_suggestion_threshold(_find_suggestion_threshold(model, data_df, genres))
    
    model_str = './data/' + str(botId) + '_model' '.pkl'

    __model_path = os.path.join(PROJECT_ROOT_PATH, model_str)

    # Save model
    joblib.dump(model, __model_path, compress=False)

def _score2prob(score):
    prob = (np.arctan(score)*2 / np.pi + 1) / 2
    prob *= 100

    return round(prob, 2)

def _find_suggestion_threshold(model, df, genres):
    '''
    Find threshold for intent suggestion feature.
    Only excecute below lines if there're more than 3 intents.
    '''
    suggestion_thres = 50
    if len(genres) > 3:
        # Compress all intent into a single column named Intents
        compressed_df = pd.DataFrame(
            {
                'Sentence': df['Sentence'].replace('_', '').to_list(),
                'Intents': [[col for col in genres if row[col] == 1] \
                            for i, row in df.iterrows()]
            }
        )
        # For each sample, get 2nd-biggest-score intent.
        results = model.predict(compressed_df["Sentence"].to_list(), oos_threshold=0)
        second_predictions = [''] * len(compressed_df)
        second_scores = [0] * len(compressed_df)
        for i, res in enumerate(results):
            for j in range(len(res)):
                if j == 1:
                    second_predictions[i] = res[j][0]
                    second_scores[i] = _score2prob(res[j][1])
                    break

        # Find which 2 intents having total largest amount and having no common sample.
        max_total_samples = 0
        most_distinct_pairs = None
        samples = [{}]  * len(genres)
        for i in range(len(genres) - 1):
            samples[i] = set(df[df[genres[i]] == 1]["Sentence"])
            for j in range(i + 1, len(genres)):
                samples[j] = set(df[df[genres[j]] == 1]["Sentence"])
                if len(samples[i]) + len(samples[j]) > max_total_samples and len(samples[i].intersection(samples[j])) == 0:
                    max_total_samples = len(samples[i]) + len(samples[j])
                    most_distinct_pairs = (genres[i], genres[j])

        if not most_distinct_pairs:
            return suggestion_thres

        # Collect all scores whose intent contradicts with labeled intents.
        group_a = {most_distinct_pairs[0]}
        group_b = {most_distinct_pairs[1]}
        unwanted_scores = []
        for i, row in compressed_df.iterrows():
            if (len(set(row["Intents"]).intersection(group_a)) > 0 and (second_predictions[i] in group_b)) \
                or (len(set(row["Intents"]).intersection(group_b)) > 0 and (second_predictions[i] in group_a)):
                unwanted_scores.append(second_scores[i])

        # Suggestion threshold is third quantile of unwanted scores.
        if len(unwanted_scores) > 3:
            suggestion_thres = np.percentile(unwanted_scores, 75)
        elif len(unwanted_scores) > 0:
            suggestion_thres = max(unwanted_scores)

    return suggestion_thres


def _get_model_path(botId):
    PROJECT_ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
    model_str = './data/' + str(botId) + '_model' '.pkl'
    __model_path = os.path.join(PROJECT_ROOT_PATH, model_str)
    return __model_path


def _load_model(botId):
    model = {}

    global intentModels
    __modelpath = _get_model_path(botId)

    if (
        not intentModels
        or not botId in intentModels
        or not intentModels[botId]
    ):
        model = joblib.load(__modelpath)
        intentModels[botId] = (model)
    else:
        # reload model into memory before predicting
        if intentModels[botId] is None:
            model = joblib.load(__modelpath)
            intentModels[botId] = (model)
        # or predicting if it has loaded before.
        else:
            model = intentModels[botId]

    return model

def load_unique_intents_from_file(filepath):
    intents = []
    with open(filepath) as f:
        lines = f.readlines()
    intents = [c.split("\n")[0] for c in lines]

    return intents

def _get_labels_path(botId):
    PROJECT_ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
    label_str = './data' + str(botId) + 'label.txt'
    __file_label =  os.path.join(PROJECT_ROOT_PATH, label_str)
    return __file_label

def predict(botId: str,
            sentences: Union[Dict, List[str]],
            oos_threshold: float):
    preds = None

    model = _load_model(botId)
    __labelpath = _get_labels_path(botId)
    if isinstance(model, Pipeline):
        intents = load_unique_intents_from_file(__labelpath)
        intents = np.array(intents)
        scores = model.decision_function(sentences)
        sorted_scores = np.array([x[::-1] for x in np.sort(scores)]) # descending
        sorted_ids = (-scores).argsort(axis=1)
        sorted_intents = np.apply_along_axis(
                                    func1d=lambda x: intents[x],
                                    axis=1,
                                    arr=sorted_ids)
        preds = [[(i, s) for i, s in zip(ii[:4], ss[:4])]
                    for ii, ss in zip(sorted_intents, sorted_scores)]
        for idx, pred in enumerate(preds):
            if _score2prob(pred[0][1]) < oos_threshold:
                preds[idx] = [("other", 100)] # 100 is a dummy number
    elif isinstance(model, SequentialModel):
        if not hasattr(model, '_use_oos'):
            model._use_oos = False
        preds = model.predict(sentences, oos_threshold)
    else:
        print("This type of model is not supported.")
        return {}

    return preds

def predict_label(botId, sentences, cutoff=-1):
    if not sentences:
        return []

    ret = []
    # default score threshold is 50%
    oos_threshold = 50
    if 0 < cutoff < 1:
        # convert to percentage
        oos_threshold = cutoff * 100
    preds = predict(botId, sentences, oos_threshold)

    # normalize model's output
    for pred, sent in zip(preds, sentences):
        tmp = []
        if isinstance(pred, list):
            for elem in pred:
                tmp.append({
                    "value": elem[0],
                    "confidence": _score2prob(elem[1]),
                    "text": sent})
            ret.append(tmp)

    return ret