from datetime import datetime
from typing import List
import uuid
from pydantic import BaseModel, EmailStr, constr

class BotBaseSchema(BaseModel):
    bot_id: int
    bot_name: str
    updated_at: str
    created_at: str
    bot_config: dict

    class Config:
        orm_mode = True

class BotResponse(BotBaseSchema):
    bot_id: int
    bot_name: str
    bot_config: dict
    updated_at: datetime
    created_at: datetime


class ListBotsResponse(BaseModel):
    status: str
    results: int
    bots: List[BotResponse]

class BotHttpResponse(BaseModel):
    status: str
    results: int
    data: BotResponse