import uuid
from .database import Base
from sqlalchemy import TIMESTAMP, Column, ForeignKey, String, Boolean, INT, VARCHAR, JSON
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

class Bot(Base):
    __tablename__ = 'bots'
    bot_id = Column(INT, primary_key=True, nullable=False)
    bot_name = Column(VARCHAR)
    bot_config = Column(JSON)
    updated_at = Column(TIMESTAMP(timezone=True))
    created_at = Column(TIMESTAMP(timezone=True))
    org_id = Column(INT, ForeignKey('organizations.org_id', ondelete='CASCADE'), nullable=False)

class Intent(Base):
    __tablename__ = 'intents'
    intent_id = Column(INT, primary_key=True, nullable=False)
    intent_name = Column(VARCHAR)
    