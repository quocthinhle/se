from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
from typing import Optional
from sqlalchemy import text, engine
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder
import os
import pandas as pd
import datetime
import json

from ..database import get_db
from ..nlu.intents.multi_intents_model import train, predict_label

class Pattern(BaseModel):
    content: str
    id: int
    entities: list


class Response(BaseModel):
    content: Optional[str]
    config: Optional[dict]
    conditions: dict
    response_id: int

class BotIntent(BaseModel):
    intent_identifier: int
    description: Optional[str]
    patterns: Optional[list]
    responses: Optional[list]
    name: str

router = APIRouter()

@router.post("/{botId}/train")
def train_bot(botId: int, db: Session = Depends(get_db)):
    sql = text("SELECT * from GetIntentDetailsByBotId(:bot_id)")
    results = db.execute(sql, {"bot_id": botId}).fetchall()
    bot_intents = [BotIntent(
        intent_identifier=row[0],
        name=row[2],
        description=row[3],
        patterns=row[4],
        responses=row[5],
    ) for row in results]

    data = jsonable_encoder(bot_intents)

    for obj in data:
        obj['intent_name'] = obj['name']
        obj['intent_patterns'] = [(pattern['content']) for pattern in obj['patterns']]
        obj.pop('patterns', None)
        obj.pop('responses', None)
        obj.pop('name', None)

    data = json.dumps(data)
    data = json.loads(data)

    unique_intents = set(intent['intent_name'] for intent in data)
    one_hot_encoding = {}
    for intent in data:
        intent_name = intent['intent_name']
        patterns = intent['intent_patterns']

        for pattern in patterns:
            if pattern in one_hot_encoding:
                one_hot_encoding[pattern][intent_name] = 1
            else:
                one_hot_encoding[pattern] = {intent_name: 1}
        for unique_intent in unique_intents:
            if unique_intent != intent_name:
                for pattern in one_hot_encoding:
                    if unique_intent not in one_hot_encoding[pattern]:
                        one_hot_encoding[pattern][unique_intent] = 0

    df = pd.DataFrame.from_dict(one_hot_encoding, orient='index')
    df = df.reset_index().rename(columns={'index': 'Sentence'})

    train(botId, df)

    return JSONResponse(content=jsonable_encoder(dict({ 'success': True, 'status': 'training' })))

class PredictModelDTO(BaseModel):
    sentences: list


@router.post("/{botId}/predict")
def get_bot_intent(botId: int,  predictBodyDto: PredictModelDTO, db: Session = Depends(get_db)):
    
    res = predict_label(botId, predictBodyDto.sentences)

    return JSONResponse(content=jsonable_encoder(dict({ 'success': True, 'data': res })))