from dotenv import load_dotenv
load_dotenv()

from fastapi import FastAPI, HTTPException
import uuid
import json
from typing import Optional
from app.routes import bots


app = FastAPI()

app.include_router(bots.router, tags=['Bots'], prefix='/api/bots')

@app.get("/hello/{name}/")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}

